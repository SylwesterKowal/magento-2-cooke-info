<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\CookieInfo\Block;

use Magento\Store\Model\ScopeInterface;

class Cookieinfo extends \Magento\Framework\View\Element\Template
{

    /**
     * Constructor
     *
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context   $context,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Store\Model\StoreManagerInterface         $storeManager,
        array                                              $data = []
    )
    {
        parent::__construct($context, $data);
        $this->scopeConfig = $scopeConfig;
        $this->storeManager = $storeManager;
    }

    /**
     * @return string
     */
    public function display($storeId = 0)
    {
        return $this->getConfigValue('cookieinfo/settings/tresc', $storeId);
    }

    public function getEnabled($storeId = 0)
    {
        return $this->getConfigValue('cookieinfo/settings/enable', $storeId);
    }

    public function getTextColor($storeId = 0)
    {
        return $this->getConfigValue('cookieinfo/settings/text_color', $storeId);
    }

    public function getBgColor($storeId = 0)
    {
        return $this->getConfigValue('cookieinfo/settings/bg_color', $storeId);
    }

    public function getLink($storeId = 0)
    {
        return $this->getConfigValue('cookieinfo/settings/link', $storeId);
    }

    public function getAnchorLink($storeId = 0)
    {
        return $this->getConfigValue('cookieinfo/settings/link_text', $storeId);
    }

    public function getStoreId()
    {
        return $this->storeManager->getStore()->getId();
    }


    public function getConfigValue($field, $storeId = 0)
    {
        $storeId = ($storeId) ? $storeId : $this->getStoreId();
        return $this->scopeConfig->getValue(
            $field,
            ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }
}

